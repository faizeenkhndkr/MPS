﻿
MPS.controller('LoginController', function ($scope, $location, $http, $q, appSetting, $window, localStorageService) {
    console.log(appSetting);


    
    var storageType = localStorageService.getStorageType();
    console.log(storageType);
    var vm = this;
    vm.signIn = signIn;
    function signIn(cred) {
        //console.log(IsValid);

        //var credentials = {
        //    username: "abc", password: "cdc"
        //};
        //console.log(credentials);
        if (cred.username != null && cred.password != null) {



            getData(cred).then(function success(response) {
                console.log(response);
                angular.forEach(response, function (value, key) {
                    localStorageService.set(key, value);
                });
                $window.location.href = '/Dashboard';
            }, function failure(response) {
                alert("Login Failed!!!");
            });
        }
        function getData(cred) {
            console.log("get");
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: appSetting.ServerPath +'/api/Authentication/validateUser',
                data: cred,
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function successCallback(response) {
                console.log(response.statusText);
                defer.resolve(response.data);
               
            }, function errorCallback(response) {
                console.log(response.statusText);
            });

            return defer.promise;
        };

    }
});