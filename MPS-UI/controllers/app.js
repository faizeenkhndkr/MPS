﻿var MPS = angular.module('MPS', ['ngRoute', 'LocalStorageModule']).constant("appSetting", { ServerPath: 'http://localhost:32980/', MyName: "Pinbaki" });;

//MPS.config(function (localStorageServiceProvider) {
//    localStorageServiceProvider
//      .setStorageType('lo');
//});


MPS.config(function ($routeProvider, $httpProvider) {
    $routeProvider
        .when("/",{
            templateUrl: "../views/Login.html",
            controller: "LoginController",
            controllerAs: 'loginCtrl'
        })
         .when("/Dashboard", {
                templateUrl: "../views/Dashboard.html",
           controller: "DashBoardController",
    controllerAs: 'dashBoardCtrl'
            })
         // Logout page
        //.when("/Logout", {                           
        //    templateUrl: "../views/logout.html",
        //    controller: "LogoutController"
        //})
        //.otherwise({ redirectTo: '/' });
});
