﻿using MPS.Providers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace MPS.Controllers
{
    public class AuthenticationController : ApiController
    {
        private static Token _myToken;
        public static string ApiUri = ConfigurationManager.AppSettings["RedirectUrl"].ToString();
        private static async Task<HttpResponseMessage> CallApiTask(string apiEndPoint, Dictionary<string, string> model = null)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ApiUri);
                client.DefaultRequestHeaders.Accept.Clear();
                if (_myToken != null)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _myToken.AccessToken);

                }
                else
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                }
                return await client.PostAsync(apiEndPoint, model != null ? new FormUrlEncodedContent(model) : null);
            }
        }

        public class UserCred
        {
           public string username { get; set; }
           public string password { get; set; }
        }

        [HttpPost]
        public async Task<IHttpActionResult> ValidateUser(UserCred credentials)
        {
            string tokenEmail = "Robert.Grow";
            string tokenPassword = "rg1234!";


            //string tokenEmail = credentials.username;
            //string tokenPassword = credentials.password;

            if (String.IsNullOrEmpty(tokenEmail) || String.IsNullOrEmpty(tokenPassword))
            {
                throw new ArgumentNullException();
            }
            var tokenModel = new Dictionary<string, string>
            {
                {"grant_type", "password" },
                { "username", tokenEmail },
                { "password", tokenPassword }
            };
            var response = await CallApiTask("api/authtoken", tokenModel);
            if (!response.IsSuccessStatusCode) { var errors = await response.Content.ReadAsStringAsync();
                throw new Exception(errors); }
            _myToken = response.Content.ReadAsAsync<Token>(new[] { new JsonMediaTypeFormatter() }).Result;
            return Ok(_myToken);
        }
    }
}
