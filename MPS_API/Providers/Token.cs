﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPS.Providers
{
    public class Token
    {
        
        [JsonProperty("Access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("UserID")]
        public string UserID { get; set; }
        [JsonProperty("Role")]
        public string Role { get; set; }
    }
}